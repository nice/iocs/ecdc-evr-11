#!/usr/bin/env iocsh.bash

require(mrfioc2)
#require(evr_seq_calc)
require(cntpstats)
require(ntpshm)

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

epicsEnvSet("IOC", "ecdc-evr-11")
epicsEnvSet("SYS", "Labs-ECDC:")
epicsEnvSet("EVR", "EVR-11")
epicsEnvSet("DEV", "TS-$(EVR)")
epicsEnvSet("SYSPV", "$(SYS)$(DEV)")
epicsEnvSet("PCIID", "5:0.0")
epicsEnvSet("CHIC_SYS", "$(SYS)")
epicsEnvSet("CHOP_DRV", "Chop-Drv-02")
epicsEnvSet("MRF_HW_DB", "evr-pcie-300dc-univ.db")
epicsEnvSet("BASEEVTNO", "21")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(essioc_DIR)/essioc.iocsh")

iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(SYSPV), EVR=EVR, PCIID=$(PCIID)")

iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(SYSPV)")

dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":00-", EVR=EVR, CODE=21, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=10000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":01-", EVR=EVR, CODE=22, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=10000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":02-", EVR=EVR, CODE=23, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=10000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":03-", EVR=EVR, CODE=24, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=10000")

# Load the sequencer configuration script
#iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc.iocsh", "P=$(SYS), R=$(CHOP_DRV), EVR=$(DEV):, BASEEVTNO=$(BASEEVTNO)")

iocshLoad("$(cntpstats_DIR)/cntpstats.iocsh","SYS=$(SYS), DEV=$(DEV)")

# Configure the EVR to write its timestamp into NTP shared memory segment 2
# and load some records to expose the NTP shared memory segment as PVs
#time2ntp("EVR", 2)
#iocshLoad("$(ntpshm_DIR)/ntpshm.iocsh", "P=$(SYS), R=$(EVR):")

iocInit()

iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), EVR=EVR")

######### INPUTS #########
# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
dbpf $(SYSPV):UnivIn-0-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-0-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB00-Src-SP 61
dbpf $(SYSPV):UnivIn-0-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-0-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-0-Code-Ext-SP 21
dbpf $(SYSPV):UnivIn-0-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-1-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-1-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB01-Src-SP 61
dbpf $(SYSPV):UnivIn-1-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-1-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-1-Code-Ext-SP 22
dbpf $(SYSPV):UnivIn-1-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-2-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-2-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB02-Src-SP 61
dbpf $(SYSPV):UnivIn-2-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-2-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-2-Code-Ext-SP 23
dbpf $(SYSPV):UnivIn-2-Code-Back-SP 0

dbpf $(SYSPV):UnivIn-3-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-3-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB03-Src-SP 61
dbpf $(SYSPV):UnivIn-3-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-3-Trig-Back-Sel "Off"
dbpf $(SYSPV):UnivIn-3-Code-Ext-SP 24
dbpf $(SYSPV):UnivIn-3-Code-Back-SP 0

dbpf $(SYSPV):EvtA-SP.OUT "@OBJ=EVR,Code=14" 
dbpf $(SYSPV):EvtA-SP.VAL 10 
dbpf $(SYSPV):EvtB-SP.OUT "@OBJ=EVR,Code=11" 
dbpf $(SYSPV):EvtB-SP.VAL 11 
dbpf $(SYSPV):EvtC-SP.OUT "@OBJ=EVR,Code=122" 
dbpf $(SYSPV):EvtC-SP.VAL 12
dbpf $(SYSPV):EvtD-SP.OUT "@OBJ=EVR,Code=13"
dbpf $(SYSPV):EvtD-SP.VAL 13
dbpf $(SYSPV):EvtE-SP.OUT "@OBJ=EVR,Code=125"
dbpf $(SYSPV):EvtE-SP.VAL 14
dbpf $(SYSPV):EvtF-SP.OUT "@OBJ=EVR,Code=15"
dbpf $(SYSPV):EvtF-SP.VAL 15
dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=EVR,Code=16"
dbpf $(SYSPV):EvtG-SP.VAL 16
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=17"
dbpf $(SYSPV):EvtH-SP.VAL 17
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=18"
dbpf $(SYSPV):EvtH-SP.VAL 18
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=19"
dbpf $(SYSPV):EvtH-SP.VAL 19
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=EVR,Code=20"
dbpf $(SYSPV):EvtH-SP.VAL 20



######### OUTPUTS #########
#Set up delay generator 0 to trigger on event 14
dbpf $(SYSPV):DlyGen-0-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-0-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-0-Evt-Trig0-SP 14

dbpf $(SYSPV):DlyGen-1-Evt-Trig0-SP 14
dbpf $(SYSPV):DlyGen-1-Width-SP 2860 #1ms
dbpf $(SYSPV):DlyGen-1-Delay-SP 0 #0ms

dbpf $(SYSPV):DlyGen-2-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-2-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-2-Evt-Trig0-SP 125

dbpf $(SYSPV):DlyGen-3-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-3-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-3-Evt-Trig0-SP 18

dbpf $(SYSPV):DlyGen-4-Width-SP 0 #1ms
dbpf $(SYSPV):DlyGen-4-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-4-Evt-Trig0-SP 0

dbpf $(SYSPV):DlyGen-5-Width-SP 0 #1ms
dbpf $(SYSPV):DlyGen-5-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-5-Evt-Trig0-SP 0

dbpf $(SYSPV):DlyGen-6-Width-SP 0 #1ms
dbpf $(SYSPV):DlyGen-6-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-6-Evt-Trig0-SP 0

dbpf $(SYSPV):DlyGen-7-Width-SP 0 #1ms
dbpf $(SYSPV):DlyGen-7-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-7-Evt-Trig0-SP 0

# 88052496/11073=7952Hz
#dbpf $(SYSPV):PS-0-Div-SP 11073 
dbpf $(SYSPV):Out-RB04-Src-Pulse-SP "Pulser 2"
dbpf $(SYSPV):Out-RB05-Src-Pulse-SP "Pulser 2"
dbpf $(SYSPV):Out-RB06-Src-Pulse-SP "Pulser 2"
dbpf $(SYSPV):Out-RB07-Src-Pulse-SP "Pulser 2"
#dbpf $(SYSPV):Out-RB04-Src-Scale-SP "Prescaler 0"
#dbpf $(SYSPV):Out-RB05-Src-Scale-SP "Prescaler 0"
#dbpf $(SYSPV):Out-RB06-Src-Scale-SP "Prescaler 0"
#dbpf $(SYSPV):Out-RB07-Src-Scale-SP "Prescaler 0"


######## Sequencer #########
dbpf $(SYSPV):EndEvtTicks 4

# Load sequencer setup
dbpf $(SYSPV):SoftSeq-0-Load-Cmd 1

# Enable sequencer
dbpf $(SYSPV):SoftSeq-0-Enable-Cmd 1

# Select run mode, "Single" needs a new Enable-Cmd every time, "Normal" needs Enable-Cmd once
dbpf $(SYSPV):SoftSeq-0-RunMode-Sel "Normal"


# Use ticks or microseconds
dbpf $(SYSPV):SoftSeq-0-TsResolution-Sel "Ticks"

# Select trigger source for soft seq 0, trigger source 0, delay gen 0
dbpf $(SYSPV):SoftSeq-0-TrigSrc-0-Sel 0

# Commit all the settings for the sequnce
# commit-cmd by evrseq!!! 
dbpf $(SYSPV):SoftSeq-0-Commit-Cmd "1"

